---
title: "About me"
image: "about.jpg"
weight: 1
---

I'm Shahrzad, an Iranian street artist. As you may know, my name referes to the storyteller of the famous strory "One Thousand and One Nights" . So I am trying to tell stories through my artworks or as we can say , I'm a street storryteller and the walls of my city are the pages of my book.
My artworks include paste-up, reverse mirror and glass painting, stencil mirror, collage, digital painting and antique mirror.

I am fascinated with mirrors, so I'm trying to bring more and more mirror-artworks to Tehran's streets.

